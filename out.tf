output hcloud_network {
  value = hcloud_network.kube.name
}

output hcloud_subnet_cidr {
  value = hcloud_network_subnet.kube_subnet.ip_range
}

output hcloud_master_internal_ip {
  value = hcloud_server_network.master[0].ip
}

output kube_public_ip {
  value = length(hcloud_load_balancer.master) > 0? hcloud_load_balancer.master[0].ipv4 : hcloud_server.master[0].ipv4_address
}

output kube_private_ip {
  value = length(hcloud_load_balancer.master) > 0? hcloud_load_balancer_network.master[0].ip : hcloud_server_network.master[0].ip
}

#output kube_config {
#  value = data.external.kube_config.result.config
#}

#output hcloud_multi_master {
#  value = var.enable_multi_master
#}

#output hcloud_k3s_project {
#  value = resource.random_string.k3s_token.id
#}

#output master_cloud_config {
#  value = data.template_file.master_cloud_config[0].rendered
#}
