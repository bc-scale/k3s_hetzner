resource "kubectl_manifest" "cluster_autoscaler" {
  depends_on = [
    null_resource.master,
    data.kubectl_path_documents.cluster_autoscaler,
  ]

  #for_each  = data.kubectl_path_documents.cluster_autoscaler.manifests
  #yaml_body = each.value

  yaml_body = element(data.kubectl_path_documents.cluster_autoscaler.documents, count.index)
  #count     = length(data.kubectl_path_documents.cluster_autoscaler.documents)

  count = length(
    flatten(
      toset([
        for f in fileset(".", data.kubectl_path_documents.cluster_autoscaler.pattern) : split("\n---\n", file(f))
        ]
      )
    )
  )


  force_new = true
  wait      = true
}
