resource "random_string" "k3s_token" {
  length  = 48
  special = false
}

resource "hcloud_ssh_key" "key" {
  name        = var.key
  public_key  = file(var.cert)
}

resource "hcloud_network" "kube" {
  name      = var.network_name
  ip_range  = var.network_range
}

resource "hcloud_network_subnet" "kube_subnet" {
  network_id    = hcloud_network.kube.id
  ip_range      = var.network_subnet
  type          = "server"
  network_zone  = "eu-central"
}

resource "hcloud_placement_group" "kube_resource_group" {
  name = var.placement_group_name
  type = "spread"
}
